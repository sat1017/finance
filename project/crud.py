import json
# import inspect


from sqlalchemy import exc


from project.database import db_session
from project.log import logger
from project.models import User, Index, PortfolioBalance

### Create ###
def create_user(user: dict):
    """Create a row in the User database

    Parameters
    ----------
    user : dict
        The user to be added

    Raises
    -------
    TypeError
        If input user keys do not match the database User object
    IntegrityError
        If user already exists
    """
    try:
        _user = User(**user)
    except TypeError as e:
        logger.error(f'TypeError: {e.args[0]}')
    else:
        try:
            db_session.add(_user)
            db_session.commit()
        except exc.IntegrityError as e:
            db_session.rollback()
            logger.info(f'User: {_user.Name} in table: {User.__tablename__} already exists')
            logger.info('DB rollback')
        else:
            logger.info(f'Added user: {_user.Name} to table: {User.__tablename__}')

            
def create_portfolio_balance(portfolio_balance: dict):
    """Create a row in the PortfolioBalance database

    Parameters
    ----------
    portfolio_balance : dict
        The portfolio to be added

    Raises
    -------
    TypeError
        If input portfolio balance keys do not match the database PortfolioBalance object
    IntegrityError
        If the portfolio balance already exists
    """
    try:
        _row = PortfolioBalance(**portfolio_balance)
    except TypeError as e:
        logger.error(f'TypeError: {e.args[0]}')
        raise
    else:
        try:
            db_session.add(_row)
            db_session.commit()
        except exc.IntegrityError as e:
            db_session.rollback()
            logger.info(f'Portfolio: {_row.PortfolioID} in table: {PortfolioBalance.__tablename__} already exists')
            logger.info('DB rollback')
        else:
            logger.info(f'Added portfolio: {_row.PortfolioID} to table: {PortfolioBalance.__tablename__}')

            
def create_index(index: dict):
    """Create a row in the Index database

    Parameters
    ----------
    index : dict
        The index to be added

    Raises
    -------
    TypeError
        If input index keys do not match the database Index object
    IntegrityError
        If index already exists
    """
    try:
        _index = Index(**index)
    except TypeError as e:
        logger.error(f'TypeError: {e.args[0]}')
    else:
        try:
            db_session.add(_index)
            db_session.commit()
        except:
            db_session.rollback()
            logger.info(f'Index: {_index.Symbol} in table: {Index.__tablename__} already exists')
            logger.info('DB rollback')
        else:
            logger.info(f'Added index: {_index.Symbol} to table: {Index.__tablename__}')


### Read ###
def check_user_id(id: int):
    """Check by ID if a user in the the User database exists

    Parameters
    ----------
    id : string
        The id to check 

    Returns
    -------
    query : object
        User with the corresponding id
    query : None
        If the user does not exist
    """
    query = db_session.query(User).filter_by(ID=id).first()
    if query:
        logger.info(f'User: {id} in table: {User.__tablename__} already exists')
        return query
    else:
        return query


def check_user_email(email: str):
    """Check by email if a user in the the User database exists

    Parameters
    ----------
    email : string
        The email to check 

    Returns
    -------
    query : object
        User with the corresponding email
    query : None
        If the user does not exist
    """
    query = db_session.query(User).filter_by(Email=email).first()
    if query:
        logger.info(f'Email: {email} in table: {User.__tablename__} already exists')
        return query
    else:
        return query


def check_user_phone(phone):
    """Check by phone if a user in the the User database exists

    Parameters
    ----------
    phone : string
        The phone number to check 

    Returns
    -------
    query : object
        User with the corresponding phone number
    query : None
        If the user does not exist
    """
    query = db_session.query(User).filter_by(Phone=phone).first()
    if query:
        logger.info(f'Phone: {phone} in table: {User.__tablename__} already exists')
        return query
    else:
        return query

    
### Update ###


### Delete ###

    
def add():
    pass
    def _index():
        pass
    def _user():
        pass
    def _company():
        pass
    def _dim_company_info():
        pass
    return


        
### below this line is not working  code ###   
class CheckUser:

    # todo query is broken and returning rollback everytime
    def email(self, email):
        self.email = email
        results = db_session.query(User.email).filter_by(email=self.email).all()
        return results
        # self.statement = select(User).filter_by(email=email)
        # self.results = db_session.execute(self.statement).all()


    def phone(self, phone):
        self.phone = phone
        self.results = db_session.query(User.phone).filter_by(phone=self.phone).all()
        if self.results is not None:
            self.results = True
            return self.results
        return self.results
        

# https://docs.sqlalchemy.org/en/14/orm/session_basics.html#using-a-sessionmaker
# context manager is not opening the connection
def foo(name, email, phone, password):
    user = User(name=name, email=email, phone=phone, password=password)
    logger.info('function called')
    try:
        with db_session() as session:
            logger.info('db opened')
            session.add(user)
            logger.info('db updated')
        logger.info('db committed and closed')
    except:
        logger.info('shit')
    else:
        logger.info('done')
        
