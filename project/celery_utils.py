# https://github.com/inveniosoftware/flask-celeryext/blob/v0.3.4/flask_celeryext/app.py#L49
from celery import current_app as current_celery_app
from celery import Task

# make_celery is a factory function that configures and then returns a Celery app instance.
# Rather than creating a new Celery instance, we used current_app so that shared tasks work
# as expected.
def make_celery(app):
    """Create a Celery application."""
    celery = current_celery_app
    
    celery.config_from_object(app.config, namespace="CELERY")

    # Set Flask application object on the Celery application.
    if not hasattr(celery, 'flask_app'):
        celery.flask_app = app

    celery.Task = AppContextTask

    return celery


class AppContextTask(Task):
    """
    Celery task running within a Flask application context.
    """

    def __call__(self, *args, **kwargs):
        # self.app is the Celery app instance
        with self.app.flask_app.app_context():
            Task.__call__(self, *args, **kwargs)
