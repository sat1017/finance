import json
import time
from random import randint
import smtplib, ssl
from email.message import EmailMessage
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import redis
from collections import namedtuple

from celery import shared_task
from celery.schedules import crontab
from flask import Blueprint, current_app, jsonify, redirect, url_for

from project.data import get_sp500_list, get_company_info, get_index_historical, get_company_historical, create_db
from project.log import logger
from project.database import db_session
from project.models import User, PortfolioBalance

tasks_blueprint = Blueprint('tasks', __name__, url_prefix='/tasks')

@shared_task(name='_get_sp500_list')
def _get_sp500_list():
    get_sp500_list()

    
@shared_task(name='_get_index_historical')
def _get_index_historical():
    get_index_historical()

    
@shared_task(name='_get_company_historical')
def _get_company_historical():
    get_company_historical()


@shared_task(name='_get_company_info')
def _get_company_info():
    get_company_info()


@shared_task(name='send_new_user_email')
def send_new_user_email(name: str, email: str):
    # https://realpython.com/python-send-email/#sending-a-plain-text-email
    app = current_app
    logger.info('Sending an email...')
    
    port = 465
    smtp_server = 'smtp.gmail.com'
    receiver_email = email
    name = name
    subject = "Thanks for creating an account!"
    text = "Welcome!"
    html = f"""\
    <html>
      <body>
        <p style="16px">Hi {name},<br>
          Welcome to <a href="#">Paper Money.</a> 
          Please navigate to your dashboard to add funds and create your portfolio.<br>
          <br>
          Sincerly,<br>
          Stephen Tomaszewski<br>
          <a href="https://www.linkedin.com/in/stephentomaszewski/">Linkedin</a>
        </p>
      </body>
    </html>
    """

    message = MIMEMultipart('alternative')
    message['Subject'] = subject
    message['To'] = receiver_email
    message['From'] = app.config['SENDER_EMAIL']
    
    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")
    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)

    # This will load the system’s trusted CA certificates, enable host name checking and certificate validation, and try to choose reasonably secure protocol and cipher settings.
    context = ssl.create_default_context()
    
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(app.config['SENDER_EMAIL'], app.config['EMAIL_PASSWORD'])
        server.sendmail(app.config['SENDER_EMAIL'], receiver_email, message.as_string())
    return 'success'


@shared_task(name='send_sold_shares_email')
def send_sold_shares_email(user_id: int, shares: int, ticker: str):
    # https://realpython.com/python-send-email/#sending-a-plain-text-email
    app = current_app
    
    email_query = db_session.query(User).\
        join(PortfolioBalance, User.ID == PortfolioBalance.PortfolioID).\
        filter(User.ID == user_id).first()
        
    logger.info('Sending an email...')
    
    port = 465
    smtp_server = 'smtp.gmail.com'
    receiver_email = email_query.Email
    name = email_query.Name
    subject = "Transacion"
    text = "you sold stuff"
    html = f"""\
    <html>
      <body>
        <p style="16px">Hi {name},<br>
          You sold {shares} of {ticker}.<br>
          <br>
          Sincerly,<br>
          Stephen Tomaszewski<br>
          <a href="https://www.linkedin.com/in/stephentomaszewski/">Linkedin</a>
        </p>
      </body>
    </html>
    """

    message = MIMEMultipart('alternative')
    message['Subject'] = subject
    message['To'] = receiver_email
    message['From'] = app.config['SENDER_EMAIL']
    
    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")
    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)
    
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(app.config['SENDER_EMAIL'], app.config['EMAIL_PASSWORD'])
        # server.sendmail(sender_email, receiver_email, message)
        server.sendmail(app.config['SENDER_EMAIL'], receiver_email, message.as_string())
    return 'success'


@shared_task(name='send_bought_shares_email')
def send_bought_shares_email(user_id: int, shares: int, ticker: str):
    # https://realpython.com/python-send-email/#sending-a-plain-text-email
    app = current_app
    
    email_query = db_session.query(User).\
        join(PortfolioBalance, User.ID == PortfolioBalance.PortfolioID).\
        filter(User.ID == user_id).first()
        
    logger.info('Sending an email...')
    
    port = 465
    smtp_server = 'smtp.gmail.com'
    receiver_email = email_query.Email
    name = email_query.Name
    subject = "Transacion"
    text = "you sold stuff"
    html = f"""\
    <html>
      <body>
        <p style="16px">Hi {name},<br>
          You bought {shares} of {ticker}.<br>
          <br>
          Sincerly,<br>
          Stephen Tomaszewski<br>
          <a href="https://www.linkedin.com/in/stephentomaszewski/">Linkedin</a>
        </p>
      </body>
    </html>
    """

    message = MIMEMultipart('alternative')
    message['Subject'] = subject
    message['To'] = receiver_email
    message['From'] = app.config['SENDER_EMAIL']
    
    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")
    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)
    
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(app.config['SENDER_EMAIL'], app.config['EMAIL_PASSWORD'])
        # server.sendmail(sender_email, receiver_email, message)
        server.sendmail(app.config['SENDER_EMAIL'], receiver_email, message.as_string())
    return 'success'


@shared_task(name='get_top_ten_gainers')
def get_top_ten_gainers():
    
    query = (
        db_session.execute(
            """WITH p AS (
            SELECT CompanyId,
            100 * (MAX(CASE WHEN rn = 1 THEN Close END) / MAX(CASE WHEN rn = 2 THEN Close END) - 1) DayGain
            FROM (
            SELECT *, ROW_NUMBER() OVER (PARTITION BY CompanyId ORDER BY Date DESC) rn
            FROM DimCompanyPrice
            )
            WHERE rn <= 2
            GROUP BY CompanyId
            ORDER BY DayGain DESC
            LIMIT 10
            )
            SELECT *
            FROM p
            JOIN Company ON p.CompanyID = Company.ID"""
        )    
    )
    Gain = namedtuple('Gain', query.keys())
    gains = [Gain(*q) for q in query.fetchall()]
    payload = [[g.Symbol, g.Security, g.DayGain] for g in gains]

    r = redis.Redis(host=current_app.config['REDIS_HOST'], port=6379, db=0)
    r.delete('top_ten_gainers')
    r.set('top_ten_gainers', json.dumps(payload))

    return 'success'


@shared_task(name='get_top_ten_losers')
def get_top_ten_losers():
    
    query = (
        db_session.execute(
            """WITH p AS (
            SELECT CompanyId,
            100 * (MAX(CASE WHEN rn = 1 THEN Close END) / MAX(CASE WHEN rn = 2 THEN Close END) - 1) DayLose
            FROM (
            SELECT *, ROW_NUMBER() OVER (PARTITION BY CompanyId ORDER BY Date DESC) rn
            FROM DimCompanyPrice
            )
            WHERE rn <= 2
            GROUP BY CompanyId
            ORDER BY DayLose ASC
            LIMIT 10
            )
            SELECT *
            FROM p
            JOIN Company ON p.CompanyID = Company.ID"""
        )    
    )
    Loser = namedtuple('Loser', query.keys())
    losers = [Loser(*q) for q in query.fetchall()]
    payload = [[l.Symbol, l.Security, l.DayLose] for l in losers]

    r = redis.Redis(host=current_app.config['REDIS_HOST'], port=6379, db=0)
    r.delete('top_ten_losers')
    r.set('top_ten_losers', json.dumps(payload))

    return 'success'

