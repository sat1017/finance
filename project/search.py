from collections import defaultdict
from datetime import date
import time


from flask import Blueprint, flash, g, request, session, render_template, url_for, redirect


from project.database import db_session
from project.models import Company, DimCompanyInfo, DimCompanyPrice, DimIndexPrice, Index
from project.log import logger

search_blueprint = Blueprint('search', __name__)

# def search():
#     q = request.args.get('q', None)
#     print(q)


def _helper(query: list, index: bool=False) -> tuple:
    _date = []
    close = []
    if index:
        for (index, dim) in query:
            _date.append(str(dim.Date))
            close.append(dim.Close)

        _date.reverse()
        close.reverse()
        first, last = close[0], close[-1]
        perf = (last-first)/first*100

        payload = {
            'Security': index.FundName,
            'Symbol': index.Symbol,
            'Current_Price': last,
            'Labels': _date,
            'Data': close,
            'Performance': perf
        }
    else:
        for (company, dim) in query:
            _date.append(str(dim.Date))
            close.append(dim.Close)

        _date.reverse()
        close.reverse()
        first, last = close[0], close[-1]
        perf = (last-first)/first*100

        payload = {
            'Security': company.Security,
            'Symbol': company.Symbol,
            'Current_Price': last,
            'Labels': _date,
            'Data': close,
            'Performance': perf
        }
        
    return payload


def check_session_for_q():
    q = request.args.get('q', None)
    if q is not None:
        q = q.upper()
        session['q'] = q
    else:
        q = session.get('q')
        
    logger.info(session['q'])

    return q


# WORKING
@search_blueprint.route('/company_overview', methods=['GET'])
def company_overview():

    # check if user is in session
    q = check_session_for_q()

    query = db_session.query(Company, DimCompanyInfo).\
        join(DimCompanyInfo, Company.ID == DimCompanyInfo.CompanyID).\
        filter(Company.Symbol==q).\
        order_by(DimCompanyInfo.ID.desc())

    # check if user searched for index instead
    if query.first() is None:
        index_query = db_session.query(Index, DimIndexPrice).\
        join(DimIndexPrice, Index.ID == DimIndexPrice.IndexID).\
        filter(Index.Symbol==q)

        if index_query.first() is None:  # user search was neither company nor index
            logger.info(f'{q} does not exist')
            flash(f"{q} does not exist. Please enter S&P 500 ticker symbols only.")
            return redirect(url_for('index.index'))
        else:                   # user searched for index
            return redirect(url_for('.index_historical_price'))

    # user searched for company
    query_price = db_session.query(Company, DimCompanyPrice).\
        join(DimCompanyPrice, Company.ID == DimCompanyPrice.CompanyID).\
        filter(Company.Symbol==q).\
        order_by(DimCompanyPrice.Date.desc()).\
        first()
    
    cur_price = query_price[1].Close
    
    logger.info(f'Table: {DimCompanyInfo.__tablename__} queried for {q}')

    company_overview_tab = {}
    
    (company, dimcompanyinfo) = query.first()
    company_overview_tab = {
        'Header': {
            'Security': company.Security,
            'Symbol': company.Symbol,
            'Current Price': cur_price
            },
        'Body': {
            'Asset Type': dimcompanyinfo.AssetType,
            'Description': dimcompanyinfo.Description,
            'CIK': dimcompanyinfo.CIK,
            'Exchange': dimcompanyinfo.Exchange,
            'Currency': dimcompanyinfo.Currency,
            'Country': dimcompanyinfo.Country,
            'Sector': dimcompanyinfo.Sector,
            'Industry': dimcompanyinfo.Industry,
            'Address': dimcompanyinfo.Address,
            'Fiscal Year End': dimcompanyinfo.FiscalYearEnd,
            'Latest Quarter': dimcompanyinfo.LatestQuarter,
            'Market Capitalization': dimcompanyinfo.MarketCapitalization
        }
    }
    # https://stackoverflow.com/questions/2241891/how-to-initialize-a-dict-with-keys-from-a-list-and-empty-value-in-python
    # {key: None for key in keys}
    return render_template('/search/company_overview.html', payload=company_overview_tab)


@search_blueprint.route('/financials', methods=['GET'])
def financials():
    
    q = check_session_for_q()
    
    query = db_session.query(Company, DimCompanyInfo).\
        join(DimCompanyInfo, Company.ID == DimCompanyInfo.CompanyID).\
        filter(Company.Symbol==q).\
        order_by(DimCompanyInfo.ID.desc())

    query_price = db_session.query(Company, DimCompanyPrice).\
        join(DimCompanyPrice, Company.ID == DimCompanyPrice.CompanyID).\
        filter(Company.Symbol==q).\
        order_by(DimCompanyPrice.Date.desc()).\
        first()
    
    cur_price = query_price[1].Close
    
    logger.info(f'Table: {DimCompanyInfo.__tablename__} queried for {q}')

    
    (company, dimcompanyinfo) = query.first()
    financials_tab = {}
    financials_tab = {
        'Header': {
            'Security': company.Security,
            'Symbol': company.Symbol,
            'Current Price': cur_price
            },
        'Body': {
            'EBITDA': dimcompanyinfo.EBITDA,
            'PE Ratio': dimcompanyinfo.PERatio,
            'PEG Ratio': dimcompanyinfo.PEGRatio,
            'Book Value': dimcompanyinfo.BookValue,
            'Dividend Per Share': dimcompanyinfo.DividendPerShare,
            'Dividend Yield': dimcompanyinfo.DividendYield,
            'EPS': dimcompanyinfo.EPS,
            'Revenue Per Share TTM': dimcompanyinfo.RevenuePerShareTTM,
            'Profit Margin': dimcompanyinfo.ProfitMargin,
            'Operating Margin TTM': dimcompanyinfo.OperatingMarginTTM,
            'Return on Assets TTM': dimcompanyinfo.ReturnOnAssetsTTM,
            'Return on Equity TTM': dimcompanyinfo.ReturnOnEquityTTM,
            'Revenue TTM': dimcompanyinfo.RevenueTTM,
            'Gross Profit TTM': dimcompanyinfo.GrossProfitTTM,
            'Diluted EPS TTM': dimcompanyinfo.DilutedEPSTTM,
            'Quarterly Earnings Growth YOY': dimcompanyinfo.QuarterlyEarningsGrowthYOY,
            'Quarterly Revenue Growth YOY': dimcompanyinfo.QuarterlyRevenueGrowthYOY,
            'Analyst Target Price': dimcompanyinfo.AnalystTargetPrice,
            'Trailing PE': dimcompanyinfo.TrailingPE,
            'Forward PE': dimcompanyinfo.ForwardPE,
            'Price To Sales Ratio TTM': dimcompanyinfo.PriceToSalesRatioTTM,
            'Price To Book Ratio': dimcompanyinfo.PriceToBookRatio,
            'EV To Revenue': dimcompanyinfo.EVToRevenue,
            'EV To EBITDA': dimcompanyinfo.EVToEBITDA
        }
    }

    return render_template('/search/financials.html', payload=financials_tab)


@search_blueprint.route('/statistics', methods=['GET'])
def statistics():

    q = check_session_for_q()
    
    query = db_session.query(Company, DimCompanyInfo).\
        join(DimCompanyInfo, Company.ID == DimCompanyInfo.CompanyID).\
        filter(Company.Symbol==q).\
        order_by(DimCompanyInfo.ID.desc())

    query_price = db_session.query(Company, DimCompanyPrice).\
        join(DimCompanyPrice, Company.ID == DimCompanyPrice.CompanyID).\
        filter(Company.Symbol==q).\
        order_by(DimCompanyPrice.Date.desc()).\
        first()
    
    cur_price = query_price[1].Close
    
    logger.info(f'Table: {DimCompanyInfo.__tablename__} queried for {q}')

    
    (company, dimcompanyinfo) = query.first()
    statistics_tab = {}
    statistics_tab = {
        'Header': {
            'Security': company.Security,
            'Symbol': company.Symbol,
            'Current Price': cur_price
            },
        'Body': {
            'Beta': dimcompanyinfo.Beta,
            '52-Week High': dimcompanyinfo._52WeekHigh,
            '52-Week Low': dimcompanyinfo._52WeekLow,
            '50-Day Moving Average': dimcompanyinfo._50DayMovingAverage,
            '200-Day Moving Average': dimcompanyinfo._200DayMovingAverage,
            'Shares Outstanding': dimcompanyinfo.SharesOutstanding,
            'Shares Float': dimcompanyinfo.SharesFloat,
            'Shares Short': dimcompanyinfo.SharesShort,
            'Shares Short Prior Month': dimcompanyinfo.SharesShortPriorMonth,
            'Short Ratio': dimcompanyinfo.ShortRatio,
            'Short Percent Outstanding': dimcompanyinfo.ShortPercentOutstanding,
            'Short Percent Float': dimcompanyinfo.ShortPercentFloat,
            'Percent Insiders': dimcompanyinfo.PercentInsiders,
            'Percent Institutions': dimcompanyinfo.PercentInstitutions,
            'Forward Annual Dividend Rate': dimcompanyinfo.ForwardAnnualDividendRate,
            'Forward Annual Dividend Yield': dimcompanyinfo.ForwardAnnualDividendYield,
            'Payout Ratio': dimcompanyinfo.PayoutRatio,
            'Dividend Date': dimcompanyinfo.DividendDate,
            'ExDividend Date': dimcompanyinfo.ExDividendDate,
            'Last Split Factor': dimcompanyinfo.LastSplitFactor,
            'Last Split Date': dimcompanyinfo.LastSplitDate,
            }
    }
    return render_template('/search/statistics.html', payload=statistics_tab)


#WORKING
@search_blueprint.route('/index_historical_price', methods=['GET'])
def index_historical_price():
    
    q = check_session_for_q()
    
    index_query = db_session.query(Index, DimIndexPrice).\
        join(DimIndexPrice, Index.ID == DimIndexPrice.IndexID).\
        filter(Index.Symbol==q).\
        order_by(DimIndexPrice.Date.desc()).\
        all()
    
    payload = _helper(index_query, index=True)

    return render_template('/search/index_historical_price.html', payload=payload)


# WORKING
@search_blueprint.route('/historical_price', methods=['GET'])
def historical_price():

    q = check_session_for_q()
    
    query_price = db_session.query(Company, DimCompanyPrice).\
        join(DimCompanyPrice, Company.ID == DimCompanyPrice.CompanyID).\
        filter(Company.Symbol==q).\
        order_by(DimCompanyPrice.Date.desc()).\
        all()
    
    logger.info(f'Table: {DimCompanyPrice.__tablename__} queried for {q}')

    payload = _helper(query_price)
    
    return render_template('/search/historical_price.html', payload=payload)


# WORKING
@search_blueprint.route('/five_days', methods=['GET'])
def five_days():

    q = check_session_for_q()

    query = db_session.query(Company, DimCompanyPrice).\
        join(DimCompanyPrice, Company.ID == DimCompanyPrice.CompanyID).\
        filter(Company.Symbol==q).\
        order_by(DimCompanyPrice.Date.desc()).\
        limit(5)
    
    logger.info(f'Table: {DimCompanyPrice.__tablename__} queried for {q}')

    # user searched for index
    if not query[:]:
        query = db_session.query(Index, DimIndexPrice).\
        join(DimIndexPrice, Index.ID == DimIndexPrice.IndexID).\
        filter(Index.Symbol==q).\
        order_by(DimIndexPrice.Date.desc()).\
        limit(5)
        
        logger.info(f'Table: {DimIndexPrice.__tablename__} queried for {q}')
        
        payload = _helper(query, index=True)
        
        return render_template('/search/index_historical_price.html', payload=payload)
        
    payload = _helper(query)
    
    return render_template('/search/historical_price.html', payload=payload)


# WORKING
@search_blueprint.route('/one_month', methods=['GET'])
def one_month():

    q = check_session_for_q()

    one_month_back = date.today()
    if one_month_back.month == 1:
        one_month_back = date(one_month_back.year-1, 12, one_month_back.day)
    else:
        one_month_back = date(one_month_back.year, one_month_back.month-1, one_month_back.day)
        
    query = db_session.query(Company, DimCompanyPrice).\
        join(DimCompanyPrice, Company.ID == DimCompanyPrice.CompanyID).\
        filter(Company.Symbol==q).\
        filter(DimCompanyPrice.Date >= one_month_back).\
        order_by(DimCompanyPrice.Date.desc())
    
    logger.info(f'Table: {DimCompanyPrice.__tablename__} queried for {q}')

    # user searched for index
    if not query[:]:
        query = db_session.query(Index, DimIndexPrice).\
        join(DimIndexPrice, Index.ID == DimIndexPrice.IndexID).\
        filter(Index.Symbol==q).\
        filter(DimIndexPrice.Date >= one_month_back).\
        order_by(DimIndexPrice.Date.desc())
        
        logger.info(f'Table: {DimIndexPrice.__tablename__} queried for {q}')
        
        payload = _helper(query, index=True)
        
        return render_template('/search/index_historical_price.html', payload=payload)
    
    payload = _helper(query)
    
    return render_template('/search/historical_price.html', payload=payload)


# WORKING
@search_blueprint.route('/six_months', methods=['GET'])
def six_months():

    q = check_session_for_q()
    
    six_months_back = date.today()
    
    for _ in range(6):
        if six_months_back.month == 1:
            six_months_back = date(six_months_back.year-1, 12, six_months_back.day)
            logger.info(six_months_back)
        else:
            six_months_back = date(six_months_back.year, six_months_back.month-1, six_months_back.day)
            logger.info(six_months_back)
            
    query = db_session.query(Company, DimCompanyPrice).\
        join(DimCompanyPrice, Company.ID == DimCompanyPrice.CompanyID).\
        filter(Company.Symbol==q).\
        filter(DimCompanyPrice.Date >= six_months_back).\
        order_by(DimCompanyPrice.Date.desc())
    
    logger.info(f'Table: {DimCompanyPrice.__tablename__} queried for {q}')

    # user searched for index
    if not query[:]:
        query = db_session.query(Index, DimIndexPrice).\
        join(DimIndexPrice, Index.ID == DimIndexPrice.IndexID).\
        filter(Index.Symbol==q).\
        filter(DimIndexPrice.Date >= six_months_back).\
        order_by(DimIndexPrice.Date.desc())
        
        logger.info(f'Table: {DimIndexPrice.__tablename__} queried for {q}')
        
        payload = _helper(query, index=True)
        
        return render_template('/search/index_historical_price.html', payload=payload)
    
    payload = _helper(query)
    
    return render_template('/search/historical_price.html', payload=payload)


# WORKING
@search_blueprint.route('/one_year', methods=['GET'])
def one_year():

    q = check_session_for_q()

    one_year_back = date(date.today().year-1, date.today().month, date.today().day)

    query = db_session.query(Company, DimCompanyPrice).\
        join(DimCompanyPrice, Company.ID == DimCompanyPrice.CompanyID).\
        filter(Company.Symbol==q).\
        filter(DimCompanyPrice.Date >= one_year_back).\
        order_by(DimCompanyPrice.Date.desc())
    
    logger.info(f'Table: {DimCompanyPrice.__tablename__} queried for {q}')

    # user searched for index
    if not query[:]:
        query = db_session.query(Index, DimIndexPrice).\
        join(DimIndexPrice, Index.ID == DimIndexPrice.IndexID).\
        filter(Index.Symbol==q).\
        filter(DimIndexPrice.Date >= one_year_back).\
        order_by(DimIndexPrice.Date.desc())
        
        logger.info(f'Table: {DimIndexPrice.__tablename__} queried for {q}')
        
        payload = _helper(query, index=True)
        
        return render_template('/search/index_historical_price.html', payload=payload)
    
    payload = _helper(query)
    
    return render_template('/search/historical_price.html', payload=payload)


# WORKING
@search_blueprint.route('/ytd', methods=['GET'])
def ytd():

    q = check_session_for_q()

    year_begin = date(date.today().year-1, 12, 31)
    
    query = db_session.query(Company, DimCompanyPrice).\
        join(DimCompanyPrice, Company.ID == DimCompanyPrice.CompanyID).\
        filter(Company.Symbol==q).\
        filter(DimCompanyPrice.Date >= year_begin).\
        order_by(DimCompanyPrice.Date.desc())
    
    logger.info(f'Table: {DimCompanyPrice.__tablename__} queried for {q}')

    # user searched for index
    if not query[:]:
        query = db_session.query(Index, DimIndexPrice).\
        join(DimIndexPrice, Index.ID == DimIndexPrice.IndexID).\
        filter(Index.Symbol==q).\
        filter(DimIndexPrice.Date >= year_begin).\
        order_by(DimIndexPrice.Date.desc())
        
        logger.info(f'Table: {DimIndexPrice.__tablename__} queried for {q}')

        payload = _helper(query, index=True)
        
        return render_template('/search/index_historical_price.html', payload=payload)
    
    payload = _helper(query)
    
    return render_template('/search/historical_price.html', payload=payload)


# WORKING
@search_blueprint.route('/max', methods=['GET'])
def _max():

    q = check_session_for_q()

    query = db_session.query(Company, DimCompanyPrice).\
        join(DimCompanyPrice, Company.ID == DimCompanyPrice.CompanyID).\
        filter(Company.Symbol==q).\
        order_by(DimCompanyPrice.Date.desc())
    
    logger.info(f'Table: {DimCompanyPrice.__tablename__} queried for {q}')

    # user searched for index
    if not query[:]:
        query = db_session.query(Index, DimIndexPrice).\
        join(DimIndexPrice, Index.ID == DimIndexPrice.IndexID).\
        filter(Index.Symbol==q).\
        order_by(DimIndexPrice.Date.desc())
        
        logger.info(f'Table: {DimIndexPrice.__tablename__} queried for {q}')

        payload = _helper(query, index=True)
        
        return render_template('/search/index_historical_price.html', payload=payload)
    
    payload = _helper(query)
    
    return render_template('/search/historical_price.html', payload=payload)
