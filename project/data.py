#!/usr/bin/env python3
from concurrent.futures import ThreadPoolExecutor
from datetime import date
import json
import os
import re
import threading
import time
import timeit


from bs4 import BeautifulSoup
import click
from flask import Blueprint, current_app
import requests
from requests.exceptions import HTTPError, Timeout
from sqlalchemy.exc import IntegrityError, SQLAlchemyError, StatementError
from werkzeug.security import check_password_hash, generate_password_hash


from project.crud import create_user, create_index, create_portfolio_balance, check_user_email
from project.database import Base, db_session, engine
from project.log import logger
from project.models import Company, DimCompanyInfo, Index, DimIndexPrice, DimCompanyPrice
from project.utils import row2dict

data_blueprint = Blueprint('data', __name__, url_prefix='/data')

# Alpha Vantage Config
# KEY_AV = os.getenv('KEY_AV')
base_url_av = 'https://www.alphavantage.co/query'

# IEX Config
# KEY_IEX_TEST = os.getenv('KEY_IEX_TEST')
# KEY_IEX = os.getenv('KEY_IEX')

# Alpha Vantage API limit = 5/min and 500/day. Great to get historical data back to 1999.

# WORKING
def create_db():
    '''Drop all tables, create new ones, and commit required data.'''

    logger.info('Started func: recreate_db')
    
    # https://docs.sqlalchemy.org/en/14/core/metadata.html
    for t in Base.metadata.sorted_tables:
        logger.info(f'Dropped table: {t.name}')
    Base.metadata.drop_all(bind=engine)
    
    Base.metadata.create_all(bind=engine)
    for t in Base.metadata.sorted_tables:
        logger.info(f'Created table: {t.name}')
        
    user = {
        'Name': 'test',
        'Email': 'test@test.com',
        'Phone': 1,
        'Password': generate_password_hash('test',method='pbkdf2:sha256', salt_length=8)
    }
    
    create_user(user)
    
    query = check_user_email(user['Email'])
    user_id = query.ID
    portfolio_balance = {}
    portfolio_balance['PortfolioID'] = user_id
    portfolio_balance['Balance'] = 0.0
    create_portfolio_balance(portfolio_balance)
    
    indices = [{'Symbol': 'VOO', 'FundName': 'S&P 500 ETF'},
                   {'Symbol': 'BND', 'FundName': 'Total Bond Market ETF'},
                   {'Symbol': 'VXUS', 'FundName': 'Total International Stock ETF'},
                   {'Symbol': 'VEA', 'FundName': 'FTSE Developed Markets ETF'},
                   {'Symbol': 'VWO', 'FundName': 'FTSE Emerging Markets ETF'},
                   {'Symbol': 'BNDX', 'FundName': 'Total International ETF'}]

    # list(map(add_index, indices))
    [create_index(i) for i in indices]

    logger.info('Finished func: recreate_db')

    return


# WORKING
# TODO: use join to filter active/non-active instead of multi set
def get_sp500_list():
    """Scrape the S&P500 constituents list from wikipedia
    
    Parameters
    ----------
    None

    Returns
    -------
    None
    """
    logger.info('Started func: get_sp500_list')
    # get html to scrape
    source = requests.get('https://en.wikipedia.org/wiki/List_of_S%26P_500_companies') 
    soup = BeautifulSoup(source.text, 'html.parser') # lxml parser is faster
    table = soup.find('table', attrs={'class': 'wikitable sortable', 'id': 'constituents'})

    # get list of symbols from db
    results = db_session.query(Company.Symbol).all()

    # https://stackoverflow.com/questions/44355850/python-sqlalchemy-convert-lists-of-tuples-to-list-of-atomic-values
    # https://stackoverflow.com/questions/37312607/python-list-comprehension-with-tuple-unpack/37312884
    symbols_in_db = set([symbol for (symbol,) in results])

    # get list of symbols from active SP500 wiki list
    # iterates through rows in table skipping header
    # find_all on every variable is N^2 time; should figure out find_next()
    symbols_active = set() #active
    for row in table.find_all('tr')[1:]:
        symbol = row.find_all('td')[0].find('a').text
        try:
            security = row.find_all('td')[1].find('a').text
        except AttributeError:
            security = row.find_all('td')[1].text
        sec_filings = row.find_all('td')[2].find('a').get('href')
        sector = row.find_all('td')[3].text
        sub_sector = row.find_all('td')[4].text
        headquarters = row.find_all('td')[5].text

        date_first_added = row.find_all('td')[6].text.strip('\n')
        if not date_first_added:
            _date = None
        else:
            _date, *_ = date_first_added.split()

        try:
            founded_raw = row.find_all('td')[8].text.split()
            if not founded_raw:
                founded = None
        except IndexError as ierr:
            founded = None
        else:
            founded, *_ = founded_raw

        active = True

        symbols_active.add(symbol)
        
        # check if in db already, else add
        if symbol not in symbols_in_db:
            company = Company(
                Symbol=symbol,
                Security=security,
                SECFilings=sec_filings,
                Sector=sector,
                SubSector=sub_sector,
                Headquarters=headquarters,
                DateFirstAdded=_date,
                Founded=founded,
                Active=active
            )
            try:
                db_session.add(company)
                db_session.commit()
            except IntegrityError as e: #shouldn't ever run since if statement
                db_session.rollback()
                logger.info('rollback: {symbol} already exists')
            except StatementError as e:
                logger.info(f'{e} error')
            else:
                logger.info(f'Added {symbol} to table: {Company.__tablename__}')
                

    # check for companies no longer in SP500
    # https://stackoverflow.com/questions/30986751/set-difference-versus-set-subtraction
    symbols_not_active = [s for s in symbols_in_db.difference(symbols_active)]
                
    for sym in symbols_not_active:
        try:
            # https://stackoverflow.com/questions/270879/efficiently-updating-database-using-sqlalchemy-orm
            logger.info(f'{sym}')
            q = db_session.query(Company).\
                filter(Company.Symbol == sym).\
                update({'Active': False})
            db_session.commit()
            logger.info(f'{sym} is not longer in the S&P 500 index')
        except:
            db_session.rollback()
            
    logger.info('Finished func: get_sp500_list')
    return


# WORKING
def get_company_info():
    """Query db for S&P 500 list, get company info via Alpha Vantage API, and update db
    
    Parameters
    ----------
    None

    Returns
    -------
    None
    """
    logger.info('Started func: get_company_info')
    
    # sqlalchemy will insery null if value is None
    # https://stackoverflow.com/questions/32959336/how-to-insert-null-value-in-sqlalchemy/48765738
    row_default = {}
    for column in DimCompanyInfo.__table__.columns:
        row_default.setdefault(column.name)
        
    # get companies from db
    sp500_companies = db_session.query(Company).filter_by(Active=True).all()
   
    # split the company list into 2 seperate api calls to avoid api limit
    api_len = len(sp500_companies)//5

    if date.today().weekday() == 0:
        sp500_companies = sp500_companies[:api_len]
    elif date.today().weekday() == 1:
        sp500_companies = sp500_companies[api_len:2*api_len]
    elif date.today().weekday() == 2:
        sp500_companies = sp500_companies[2*api_len:3*api_len]
    elif date.today().weekday() == 3:
        sp500_companies = sp500_companies[3*api_len:4*api_len]
    elif date.today().weekday() == 4:
        sp500_companies = sp500_companies[4*api_len:]
       
    if not sp500_companies:
        logger.warning(f"{Company.__tablename__} table is empty")
        return f'{Company.__tablename__} table is empty.'

    sp500_gen = (row2dict(q) for q in sp500_companies)
     
    # create and send request to AV API
    # no need to thread since AV allows 12 requests/min
    # iter over list of companies and create dict of keys to send in a request
    for company in sp500_gen:
        # sleep to avoid api throttle
        time.sleep(15)
        
        # create url to send in a request
        payload = {
            'function': 'OVERVIEW',
            'symbol': re.sub('[.]', '-', company['Symbol']),
            'apikey': current_app.config['KEY_AV']
        }

        # send requests
        try:
            logger.info(f"{company['Symbol']} ...")
            _r = requests.get(base_url_av, params=payload, timeout=30)
            _r.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            logger.error("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            logger.error("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            logger.error(f"Timeout Error: for {company['symbol']}", errt)
        except requests.exceptions.RequestException as err:
            logger.error("Error: Something Else", err)
        else:
            r = _r.json()

        try:
            _ = r.get('Symbol').lower()
            # if _ != company['Symbol'].lower():
            #     logger.error(f"API call for {company['Symbol']} did not return expected data")
            #     continue
        except AttributeError as aerr:
            logger.error("API limit reached")
            continue

         # remove keys not needed in db
        try:
            r.pop('Symbol')
            r.pop('Name')
        except KeyError as kerr:
            logger.error(f"{company['Symbol']} response does not have valid keys")
            continue
        
        
        # set up new dict
        new_row = row_default.copy()
        new_row.pop('ID')
        new_row['CompanyID'] = str(company.get('ID'))

        # clean up data types and keys before inserting
        # https://stackoverflow.com/questions/4406501/change-the-name-of-a-key-in-dictionary
        # need to change key name to not start with a number for python variables
        r['_52WeekHigh'] = r.pop('52WeekHigh', 'None')
        r['_52WeekLow'] = r.pop('52WeekLow', 'None')
        r['_50DayMovingAverage'] = r.pop('50DayMovingAverage', 'None')
        r['_200DayMovingAverage'] = r.pop('200DayMovingAverage', 'None')
        
        # need to convert string to date if value exits
        _w = r.pop('LatestQuarter', 'None')
        if _w != 'None':
            r['LatestQuarter'] = date.fromisoformat(_w)
            
        _x = r.pop('DividendDate', 'None')
        if _x != 'None':
            r['DividendDate'] = date.fromisoformat(_x)
            
        _y = r.pop('ExDividendDate', 'None')
        if _y != 'None':
            r['ExDividendDate'] = date.fromisoformat(_y)
            
        #TODO: after refactoring DimCompanyInfo to a dict need to handle default Null values for LastSplitFactor, etc.
        _z = r.pop('LastSplitDate', 'None')
        if _z != 'None':
            r['LastSplitDate'] = date.fromisoformat(_z)
        
        # if key in default dict == key in r_json, update
        # else leave as None
        for key, value in r.items():
            if value != 'None' and key != 'Note' and key in r.keys() and value != '-': # Note will be sent if hit API limit
                new_row[key] = r[key]

        # https://stackoverflow.com/questions/31750441/generalised-insert-into-sqlalchemy-using-dictionary/31756880
        row = DimCompanyInfo(**new_row)

        try:
            db_session.add(row)
            db_session.commit()
            logger.info("Inserted 1 record for " + f"{company['Symbol']}")
        except IntegrityError as e:
            db_session.rollback()
            logger.info("Rollback: " + f"{company['Symbol']}" + " already exists")
        except ValueError as e:
            db_session.rollback()
            logger.info(f'{e}')
        except StatementError as e:
            logger.info(f'{e} error')

    logger.info('Finished func: get_company_info')

    return 0


# WORKING
def get_index_historical():
    """Query db for index list, get index info via Alpha Vantage API, and update db
    
    Parameters
    ----------
    None

    Returns
    -------
    None
    """
    
    logger.info(f'Started func: get_index_historical')
    
    row = {}
    for column in DimIndexPrice.__table__.columns:
        row.setdefault(column.name)
        
    query = db_session.query(Index).all()
    # why do I convert row object to dict?
    # makes attribute retrival annoying
    indices_gen = (row2dict(q) for q in query) ##############

    for index in indices_gen:
        time.sleep(15)
        
        payload = {
            'function': 'TIME_SERIES_DAILY_ADJUSTED',
            'symbol': index['Symbol'],
            'outputsize': 'full',
            'datatype': 'json',
            'apikey': current_app.config['KEY_AV']
        }
        
        try:
            _r = requests.get(base_url_av, params=payload, timeout=10.0)
            _r.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            logger.error("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            logger.error("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            logger.error("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            logger.error("Error: Something Else", err)
        else:
            r = _r.json()

        # update the index key since it won't change inside the loop
        row['IndexID'] = str(index.get('ID'))
        
        if r is None:
            logger.error(f"API call for {index['Symbol']} returned None")
            continue
        
        try:
            _ = r.get('Meta Data').get('2. Symbol').lower()
            # if _ != index['Symbol'].lower():
            #     logger.error(f"API call for {index['Symbol']} did not return expected data")
            #     continue
        except AttributeError as aerr:
            logger.error("API limit reached or API did not return the expected data")
            continue

        # skip database columns that don't need to be updated from the _ts_key values
        _db_ts_keys = [column.name for column in DimIndexPrice.__table__.columns][3:]

        # get the last price date from the db
        query = db_session.query(DimIndexPrice).\
            filter_by(IndexID=str(index.get('ID'))).\
            order_by(DimIndexPrice.Date.desc()).first()
        if query is not None:
            last_update = query.Date
        else:                   # if db is empty use min date as default
            last_update = date.min

        rows = []
        for date_key, _ts_key in r.get('Time Series (Daily)').items():
            # create new base copy of each index, update, and add to list of rows
            new_row = row.copy()
            # check if the date is new; else skip
            if date.fromisoformat(date_key) > last_update:
                new_row['Date'] = date.fromisoformat(date_key)
                _ts = list(zip(_db_ts_keys, _ts_key.values()))
                new_row.update(dict((_ts)))
                rows.append(new_row)
                
        try:
            n = len(rows)
            t0 = time.time()
            db_session.bulk_insert_mappings(DimIndexPrice, rows)
            logger.info("Inserted " + str(n) + " records for "
                        + f"{index['Symbol']} in " +  str(time.time() - t0) + " secs")
            db_session.commit()
        except Exception as e:
            logger.error(f'{e.args[0]}')

    logger.info(f'Finished func: get_index_historical')                 

    return 0


# WORKING
def get_company_historical():
    """Query db for index list, get index info via Alpha Vantage API, and update db
    
    Parameters
    ----------
    None

    Returns
    -------
    None
    """
    
    logger.info(f'Started func: get_company_historical')

    query = db_session.query(Company).all()

    # split workload into two days to avoid API limit
    api_len = len(query)//2
    if (date.today().day % 2) == 0:
        query = query[:api_len]
    else:
        query = query[api_len:]
        
    company_gen = (row2dict(q) for q in query)

    # logger.info(api_len)
    
    row = {}
    for column in DimCompanyPrice.__table__.columns:
        row.setdefault(column.name)
        
    for company in company_gen:
        time.sleep(15)
        
        _symbol = company['Symbol']
        
        payload = {
            'function': 'TIME_SERIES_DAILY_ADJUSTED',
            'symbol': re.sub('[.]', '-', company['Symbol']),
            'outputsize': 'full',
            'datatype': 'json',
            'apikey': current_app.config['KEY_AV']
        }

        # make a request
        try:
            _r = requests.get(base_url_av, params=payload, timeout=10.0)
            _r.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            logger.error("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            logger.error("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            logger.error("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            logger.error("Error: Something Else", err)
        else:
            r = _r.json()

        # update the index key since it won't change inside the loop
        row['CompanyID'] = str(company.get('ID'))
        
        # error handling
        try:
            _ = r.get('Meta Data').get('2. Symbol').lower()
            # if _ != company['Symbol'].lower():
            #     logger.error(f"API call for {company['Symbol']} did not return expected data")
            #     continue
        except AttributeError as aerr:
            logger.error("API limit reached or API did not return the expected data")
            continue     
        
        # skip database columns that don't need to be updated from the _ts_key values
        _db_ts_keys = [column.name for column in DimCompanyPrice.__table__.columns][3:]

        # get the last price date from the db
        query = db_session.query(DimCompanyPrice).\
            filter_by(CompanyID=str(company.get('ID'))).\
            order_by(DimCompanyPrice.Date.desc()).first()
        if query is not None:
            last_update = query.Date
        else:                   # if db is empty use min date as default
            last_update = date.min

        rows = []
        for date_key, _ts_key in r.get('Time Series (Daily)').items():
            # create new base copy of each index, update, and add to list of rows
            new_row = row.copy()
            if date.fromisoformat(date_key) > last_update:
                new_row['Date'] = date.fromisoformat(date_key)
                _ts = list(zip(_db_ts_keys, _ts_key.values()))
                new_row.update(dict((_ts)))
                rows.append(new_row)

        # bulk insert into db
        try:
            n = len(rows)
            t0 = time.time()
            db_session.bulk_insert_mappings(DimCompanyPrice, rows)
            logger.info("Inserted " + str(n) + " records for "
                        + f"{company['Symbol']} in " +  str(time.time() - t0) + " secs")
            db_session.commit()
        except Exception as e:
            logger.info(f'{e.args[0]}')

    logger.info(f'Finished func: get_company_historical')
    
    return 0
