from flask import current_app as app


from project.data import get_sp500_list, get_company_info, get_index_historical, get_company_historical, create_db
from project.tasks import get_top_ten_gainers, get_top_ten_losers
from project.log import logger

# will throw error if app.cli.command function name is same as module function name

# https://stackoverflow.com/questions/57202736/where-should-i-implement-flask-custom-commands-cli
# https://flask.palletsprojects.com/en/1.1.x/cli/


@app.cli.command('_create_db')
def _create_db():
    '''Drop all tables & create new tables.'''
    create_db()
    return


@app.cli.command('_get_sp500_list')
def _get_sp500_list():
    '''Scrape the S&P500 constituents list from wikipedia.'''
    get_sp500_list()
    return


@app.cli.command('_get_index_historical')
def _get_index_historical():
    '''Get index price data from Alpha Vantage.'''
    get_index_historical()
    return


@app.cli.command('_get_company_historical')
def _get_company_historical():
    '''Get company price data from Alpha Vantage.'''
    get_company_historical()
    return


@app.cli.command('_get_company_info')
def _get_company_info():
    '''Get company info from Alpha vantage.'''
    get_company_info()
    return


@app.cli.command('_init_app')
def _init_app():
    '''Initialize the app.'''
    create_db()
    get_sp500_list()
    get_index_historical()
    # get_company_info()
    # get_company_historical()
    return


@app.cli.command('get_top_ten_gainers')
def _get_top_ten_gainers():
    get_top_ten_gainers()
    return


@app.cli.command('get_top_ten_losers')
def _get_top_ten_losers():
    get_top_ten_losers()
    return
