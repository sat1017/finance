from sqlalchemy import Boolean, Column, Date, ForeignKey, Integer, Numeric, String, Float
from sqlalchemy.orm import relationship

from project.database import Base


# great example on SQLAlchemy overview
# https://hackersandslackers.com/sqlalchemy-data-models
class User(Base):
    __tablename__ = 'User'

    ID = Column(Integer, primary_key=True, autoincrement=True)
    Name = Column(String(50), nullable=False)
    Email = Column(String(120), unique=True, nullable=False)
    Phone = Column(Integer, unique=True, nullable=False)
    Password = Column(String(255), nullable=False)

    def __init__(self, Name, Email, Phone, Password):
        self.Name = Name
        self.Email = Email
        self.Phone = Phone
        self.Password = Password
        
    def __repr__(self):
        return '<User %r>' % (self.Name)


class Company(Base):

    __tablename__ = 'Company'
    
    ID = Column(Integer, primary_key=True, autoincrement=True)
    Symbol = Column(String(4), unique=True, nullable=False)
    Security = Column(String(120), nullable=False)
    SECFilings = Column(String(255), nullable=False)
    Sector = Column(String(255), nullable=False)
    SubSector = Column(String(255), nullable=False)
    Headquarters = Column(String(255), nullable=False)
    DateFirstAdded = Column(String(255))
    Founded = Column(String(255))
    Active = Column(Boolean, nullable=False)

    def __init__(self,
                 Symbol,
                 Security,
                 SECFilings,
                 Sector,
                 SubSector,
                 Headquarters,
                 DateFirstAdded,
                 Founded,
                 Active):
        self.Symbol = Symbol
        self.Security = Security
        self.SECFilings = SECFilings
        self.Sector = Sector
        self.SubSector = SubSector
        self.Headquarters = Headquarters
        self.DateFirstAdded = DateFirstAdded
        self.Founded = Founded
        self.Active = Active
        
    def __repr__(self):
        return '<Company %r>' % (self.Symbol)


class DimCompanyInfo(Base):

    __tablename__ = 'DimCompanyInfo'

    ID = Column(Integer, primary_key=True, autoincrement=True)
    CompanyID = Column(Integer, ForeignKey('Company.ID'), nullable=False)
    AssetType = Column(String(255), nullable=True)
    Description = Column(String(3000), nullable=True)
    CIK = Column(Integer, nullable=True)
    Exchange = Column(String(10), nullable=True)
    Currency = Column(String(10), nullable=True)
    Country = Column(String(20), nullable=True)
    Sector = Column(String(255), nullable=True)
    Industry = Column(String(255), nullable=True)
    Address = Column(String(255), nullable=True)
    # FullTimeEmployees = Column(Integer, nullable=True)
    FiscalYearEnd = Column(String(255), nullable=True)
    LatestQuarter = Column(Date(), nullable=True) # date
    MarketCapitalization = Column(Integer, nullable=True)
    EBITDA = Column(Integer, nullable=True)
    PERatio = Column(Numeric(), nullable=True)
    PEGRatio = Column(Numeric(), nullable=True)
    BookValue = Column(Numeric(), nullable=True)
    DividendPerShare = Column(Numeric(), nullable=True)
    DividendYield = Column(Numeric(), nullable=True)
    EPS = Column(Numeric(), nullable=True)
    RevenuePerShareTTM = Column(Numeric(), nullable=True)
    ProfitMargin = Column(Numeric(), nullable=True)
    OperatingMarginTTM = Column(Numeric(), nullable=True)
    ReturnOnAssetsTTM = Column(Numeric(), nullable=True)
    ReturnOnEquityTTM = Column(Numeric(), nullable=True)
    RevenueTTM = Column(Integer, nullable=True)
    GrossProfitTTM = Column(Integer, nullable=True)
    DilutedEPSTTM = Column(Numeric(), nullable=True)
    QuarterlyEarningsGrowthYOY = Column(Numeric(), nullable=True)
    QuarterlyRevenueGrowthYOY = Column(Numeric(), nullable=True)
    AnalystTargetPrice = Column(Numeric(), nullable=True)
    TrailingPE = Column(Numeric(), nullable=True)
    ForwardPE = Column(Numeric(), nullable=True)
    PriceToSalesRatioTTM = Column(Numeric(), nullable=True)
    PriceToBookRatio = Column(Numeric(), nullable=True)
    EVToRevenue = Column(Numeric(), nullable=True)
    EVToEBITDA = Column(Numeric(), nullable=True)
    Beta = Column(Numeric(), nullable=True)
    _52WeekHigh = Column(Numeric(), nullable=True)
    _52WeekLow = Column(Numeric(), nullable=True) 
    _50DayMovingAverage = Column(Numeric(), nullable=True)
    _200DayMovingAverage = Column(Numeric(), nullable=True)
    SharesOutstanding = Column(Integer, nullable=True)
    SharesFloat = Column(Integer, nullable=True)
    SharesShort = Column(Integer, nullable=True)
    SharesShortPriorMonth = Column(Integer, nullable=True)
    ShortRatio = Column(Numeric(), nullable=True)
    ShortPercentOutstanding = Column(Numeric(), nullable=True)
    ShortPercentFloat = Column(Numeric(), nullable=True)
    PercentInsiders = Column(Numeric(), nullable=True)
    PercentInstitutions = Column(Numeric(), nullable=True)
    ForwardAnnualDividendRate = Column(Numeric(), nullable=True)
    ForwardAnnualDividendYield = Column(Numeric(), nullable=True)
    PayoutRatio = Column(Numeric(), nullable=True)
    DividendDate = Column(Date(), nullable=True) # date
    ExDividendDate = Column(Date(), nullable=True) # date
    LastSplitFactor = Column(String(255), nullable=True)
    LastSplitDate = Column(Date(), nullable=True) # date

    company = relationship("Company", backref="DimCompanyInfo")
    
    def __init__(self,
                 CompanyID,
                 AssetType,
                 Description,
                 CIK,
                 Exchange,
                 Currency,
                 Country,
                 Sector,
                 Industry,
                 Address,
                 FiscalYearEnd,
                 LatestQuarter,
                 MarketCapitalization,
                 EBITDA,
                 PERatio,
                 PEGRatio,
                 BookValue,
                 DividendPerShare,
                 DividendYield,
                 EPS,
                 RevenuePerShareTTM,
                 ProfitMargin,
                 OperatingMarginTTM,
                 ReturnOnAssetsTTM,
                 ReturnOnEquityTTM,
                 RevenueTTM,
                 GrossProfitTTM,
                 DilutedEPSTTM,
                 QuarterlyEarningsGrowthYOY,
                 QuarterlyRevenueGrowthYOY,
                 AnalystTargetPrice,
                 TrailingPE,
                 ForwardPE,
                 PriceToSalesRatioTTM,
                 PriceToBookRatio,
                 EVToRevenue,
                 EVToEBITDA,
                 Beta,
                 _52WeekHigh,
                 _52WeekLow,
                 _50DayMovingAverage,
                 _200DayMovingAverage,
                 SharesOutstanding,
                 SharesFloat,
                 SharesShort,
                 SharesShortPriorMonth,
                 ShortRatio,
                 ShortPercentOutstanding,
                 ShortPercentFloat,
                 PercentInsiders,
                 PercentInstitutions,
                 ForwardAnnualDividendRate,
                 ForwardAnnualDividendYield,
                 PayoutRatio,
                 DividendDate,
                 ExDividendDate,
                 LastSplitFactor,
                 LastSplitDate):         
        self.CompanyID = CompanyID
        self.AssetType = AssetType
        self.Description = Description
        self.CIK = CIK
        self.Exchange = Exchange
        self.Currency = Currency
        self.Country = Country 
        self.Sector = Sector
        self.Industry = Industry
        self.Address = Address
        self.FiscalYearEnd = FiscalYearEnd 
        self.LatestQuarter = LatestQuarter 
        self.MarketCapitalization = MarketCapitalization
        self.EBITDA = EBITDA
        self.PERatio = PERatio
        self.PEGRatio = PEGRatio 
        self.BookValue = BookValue
        self.DividendPerShare = DividendPerShare
        self.DividendYield = DividendYield
        self.EPS = EPS
        self.RevenuePerShareTTM = RevenuePerShareTTM
        self.ProfitMargin = ProfitMargin
        self.OperatingMarginTTM = OperatingMarginTTM
        self.ReturnOnAssetsTTM = ReturnOnAssetsTTM
        self.ReturnOnEquityTTM = ReturnOnEquityTTM
        self.RevenueTTM = RevenueTTM
        self.GrossProfitTTM = GrossProfitTTM
        self.DilutedEPSTTM = DilutedEPSTTM
        self.QuarterlyEarningsGrowthYOY = QuarterlyEarningsGrowthYOY
        self.QuarterlyRevenueGrowthYOY = QuarterlyRevenueGrowthYOY
        self.AnalystTargetPrice = AnalystTargetPrice
        self.TrailingPE = TrailingPE
        self.ForwardPE = ForwardPE
        self.PriceToSalesRatioTTM = PriceToSalesRatioTTM
        self.PriceToBookRatio = PriceToBookRatio
        self.EVToRevenue = EVToRevenue
        self.EVToEBITDA = EVToEBITDA
        self.Beta = Beta
        self._52WeekHigh = _52WeekHigh
        self._52WeekLow = _52WeekLow
        self._50DayMovingAverage = _50DayMovingAverage
        self._200DayMovingAverage = _200DayMovingAverage
        self.SharesOutstanding = SharesOutstanding
        self.SharesFloat = SharesFloat
        self.SharesShort = SharesShort
        self.SharesShortPriorMonth = SharesShortPriorMonth
        self.ShortRatio = ShortRatio
        self.ShortPercentOutstanding = ShortPercentOutstanding
        self.ShortPercentFloat = ShortPercentFloat
        self.PercentInsiders = PercentInsiders
        self.PercentInstitutions = PercentInstitutions
        self.ForwardAnnualDividendRate = ForwardAnnualDividendRate
        self.ForwardAnnualDividendYield = ForwardAnnualDividendYield
        self.PayoutRatio = PayoutRatio
        self.DividendDate = DividendDate
        self.ExDividendDate = ExDividendDate
        self.LastSplitFactor = LastSplitFactor
        self.LastSplitDate = LastSplitDate
        
    def __repr__(self):
        return '<DimCompanyInfo %r>' % (self.CompanyID)

class DimCompanyPrice(Base):

    __tablename__ = 'DimCompanyPrice'

    ID = Column(Integer, primary_key=True, autoincrement=True)
    CompanyID = Column(Integer, ForeignKey('Company.ID'), nullable=False)
    Date = Column(Date(), nullable=False)
    Open = Column(Float, nullable=False)
    High = Column(Float, nullable=False)
    Low =Column(Float, nullable=False)
    Close = Column(Float, nullable=False)
    # CloseAdjusted = Column(Float, nullable=False)
    Volume = Column(Integer, nullable=False)
    # DividendAmount = Column(Float, nullable=False)
    # SplitCoefficient = Column(Float, nullable=False)

    def __init__(self,
                 CompanyID,
                 Date,
                 Open,
                 High,
                 Low,
                 Close,
                 Volume):
        self.CompanyID = CompanyID
        self.Date = Date
        self.Open = Open
        self.High = High
        self.Low = Low
        self.Close = Close
        self.Volume = Volume
                 
    def __repr__(self):
        return '<DimCompanyPrice %r %s>' % (self.CompanyID, self.Date)

    
class Index(Base):

    __tablename__ = 'Index'

    ID = Column(Integer, primary_key=True, autoincrement=True)
    Symbol = Column(String(4), unique=True, nullable=False)
    FundName = Column(String(120), unique=True, nullable=False)

    def __init__(self, Symbol, FundName):
        self.Symbol = Symbol
        self.FundName = FundName

    def __repr__(self):
        return '<Index %r>' % (self.Symbol)

    
class DimIndexPrice(Base):

    __tablename__ = 'DimIndexPrice'

    ID = Column(Integer, primary_key=True, autoincrement=True)
    IndexID = Column(Integer, ForeignKey('Index.ID'), nullable=False)
    Date = Column(Date(), nullable=False)
    Open = Column(Float, nullable=False)
    High = Column(Float, nullable=False)
    Low =Column(Float, nullable=False)
    Close = Column(Float, nullable=False)
    # CloseAdjusted = Column(Float, nullable=False)
    Volume = Column(Integer, nullable=False)
    # DividendAmount = Column(Float, nullable=False)
    # SplitCoefficient = Column(Float, nullable=False)

    def __init__(self,
                 IndexID,
                 Date,
                 Open,
                 High,
                 Low,
                 Close,
                 Volume):
        self.IndexID = IndexID
        self.Date = Date
        self.Open = Open
        self.High = High
        self.Low = Low
        self.Close = Close
        self.Volume = Volume

    def __repr__(self):
        return '<DimIndexPrice %r %s>' % (self.IndexID, self.Date)

    
class Portfolio(Base):

    __tablename__ = 'Portfolio'

    ID = Column(Integer, primary_key=True, autoincrement=True)
    PortfolioID = Column(Integer, ForeignKey('User.ID'), nullable=False)
    Date = Column(Date(), nullable=False)
    Symbol = Column(String(4), nullable=False)
    NumberShares = Column(Float, nullable=False)
    TotalPrice = Column(Float, nullable=False)
    
    def __init__(self,
                 PortfolioID,
                 Date,
                 Symbol,
                 NumberShares,
                 TotalPrice):
        self.PortfolioID = PortfolioID
        self.Date = Date
        self.Symbol = Symbol
        self.NumberShares = NumberShares
        self.TotalPrice = TotalPrice


    def __repr__(self):
        return '<Portfolio %r %s>' % (self.PortfolioID, self.Symbol)


class PortfolioBalance(Base):

    __tablename__ = 'PortfolioBalance'

    ID = Column(Integer, primary_key=True, autoincrement=True)
    PortfolioID = Column(Integer, ForeignKey('User.ID'), nullable=False)
    Balance = Column(Float, nullable=False)
    
    def __init__(self,
                 PortfolioID,
                 Balance):
        self.PortfolioID = PortfolioID
        self.Balance = Balance

    def __repr__(self):
        return '<Portfolio %r %r>' % (self.PortfolioID, self.Balance)
