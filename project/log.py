import logging
from logging import handlers
# Logging Best Practices:
# Capitalize first word of the line and do not include period
# when describing db actions use Create, Read, Update, Delete
# use past tense when describing the action
# use Start and Finish for functions
# prefix tables, functions, columns, etc. to make log less verbose
# add space after colon


LOG_FILENAME = 'logs/project.log'
# https://stackoverflow.com/questions/3630774/logging-remove-inspect-modify-handlers-configured-by-fileconfig
# need to run this every time since I changed class attributes of logger
root = logging.getLogger()
for h in root.handlers:
    root.removeHandler(h)
    h.close()
    
# logging.basicConfig(force=True)

# create logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# create console and file handler and set level to info
fh = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=250000, backupCount=5)
fh.setLevel(logging.INFO)
# ch = logging.StreamHandler()
# ch.setLevel(logging.INFO)

# create formatter
ffh = logging.Formatter('[%(asctime)s][%(module)-12s][%(levelname)-8s] %(message)s')
# fch = logging.Formatter('%(module)-12s : %(levelname)-8s : %(message)s')

# add formatter handlers
fh.setFormatter(ffh)
# ch.setFormatter(fch)

# add handler to logger
logger.addHandler(fh)
# logger.addHandler(ch)