import os
from pathlib import Path

from celery.schedules import crontab

# from dotenv import load_dotenv
# load_dotenv()


class BaseConfig:
    """Base configuration"""
    BASE_DIR = Path(__file__).parent.parent

    # https://stackoverflow.com/questions/16924471/difference-between-os-getenv-and-os-environ-get
    FLASK_APP = os.getenv('FLASK_APP')
    TESTING = False

    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL", "redis://127.0.0.1:6379/0")
    CELERY_RESULT_BACKEND = os.environ.get("CELERY_RESULT_BACKEND", "redis://127.0.0.1:6379/0")
    CELERY_TIMEZONE = "US/Pacific"
    
    CELERY_BEAT_SCHEDULE = {
        'task-one': {           # https://crontab.guru/#0_0_*_*_*
            'task': '_get_sp500_list',
            'schedule': crontab(minute=0, hour=0, day_of_month='*', month_of_year='*', day_of_week='*'),
        },
        'task-two': {           # https://crontab.guru/#0_0_*_*_1-5
            'task': '_get_index_historical',
            'schedule': crontab(minute=0, hour=0, day_of_month='*', month_of_year='*', day_of_week='*'),
        },
        'task-three': {         # https://crontab.guru/#0_0_*_*_1-5
            'task': '_get_company_historical',
            'schedule': crontab(minute=10, hour=0, day_of_month='*', month_of_year='*', day_of_week='*'),
        },
        'task-four': {          # https://crontab.guru/#0_0_*_*/3_0,6
            'task': '_get_company_info',
            'schedule': crontab(minute=30, hour=1, day_of_month='*',month_of_year='*', day_of_week='1-5'),
        },
        'task-five': {          # https://crontab.guru/#0_0_*_*/3_0,6
            'task': 'get_top_ten_gainers',
            'schedule': crontab(minute=0, hour=2, day_of_month='*',month_of_year='*', day_of_week='1-5'),
        },
        'task-six': {          # https://crontab.guru/#0_0_*_*/3_0,6
            'task': 'get_top_ten_losers',
            'schedule': crontab(minute=0, hour=2, day_of_month='*',month_of_year='*', day_of_week='1-5'),
        },

    }
    SECRET_KEY = os.getenv('SECRET_KEY')
    LOG_FILENAME = os.getenv('LOG_FILENAME')

    SENDER_EMAIL = os.getenv('SENDER_EMAIL')
    EMAIL_PASSWORD = os.getenv('EMAIL_PASSWORD')

    KEY_AV = os.getenv('KEY_AV')

    REDIS_HOST = os.getenv("REDIS_HOST", "redis_1")

    
class DevelopmentConfig(BaseConfig):
    KEY_IEX_TEST = os.getenv('KEY_IEX_TEST')
    # FLASK_ENV = 'development'
    DEBUG = True

    
class TestingConfig(BaseConfig):
    TESTING = True
    FLASK_ENV = 'development'


class ProductionConfig(BaseConfig):
    FLASK_ENV = 'production'


config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
}
