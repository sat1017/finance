def convert(tup, di={}):
    di = dict(tup)
    return di


# https://stackoverflow.com/questions/1958219/convert-sqlalchemy-row-object-to-python-dict
def row2dict(row) -> dict:
    d = {}
    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))

    return d


# https://stackoverflow.com/questions/46794115/passing-a-tuple-in-args
def unpack(obj):
    # can use dict keys and values to generalize code
    # print(obj.__dict__.items())
    data = dict(symbol = obj.symbol,
                security = obj.security,
                sec_filings = obj.sec_filings,
                sector = obj.sector,
                sub_sector = obj.sub_sector,
                headquarters = obj.headquarters,
                date_first_added = obj.date_first_added)
        
    return data
    # return json.dumps([symbol, security, sector, sub_sector, headquarters, date_first_added])