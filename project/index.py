from datetime import date
import json
from calendar import monthrange
import time
import redis
from collections import namedtuple

from flask import Blueprint, render_template, current_app

from project.database import db_session
from project.models import Company, DimIndexPrice, DimCompanyPrice, Index
from project.utils import row2dict
from project.log import logger
from sqlalchemy import text

index_blueprint = Blueprint('index', __name__)

@index_blueprint.route('/', methods=['GET'])
def index():
    """
    Gets index price data and sp500 comp list from database
    """
    index_payload, sp500_payload, total_payload = {}, {}, {}
    struc = {
        'current_price': None,
        'percent_change': None,
        'ytd_percent_change': None
    }
        
    def _return_price_1day():

        # get all indexes 
        query = (
            db_session.query(Index).all()
        )

        l = []
        for row in query:
            
            l.append(row.Symbol)
            
            q = (
                db_session.query(DimIndexPrice).\
                filter(DimIndexPrice.IndexID==row.ID).\
                order_by(DimIndexPrice.Date.desc()).\
                limit(2)
            )
            
            index_payload[str(row.Symbol)] = struc.copy()
            
            old_price = q[1].Close
            cur_price = q[0].Close
            
            index_payload[str(row.Symbol)]['percent_change'] = (cur_price - old_price) / old_price * 100
            index_payload[str(row.Symbol)]['current_price'] = cur_price
            
        index_payload['meta_data'] = l
        
        return 0

    
    def _top_ten_gainers():

        t0 = time.perf_counter()
        
        r = redis.Redis(host=current_app.config['REDIS_HOST'], port=6379, db=0)
        payload = r.get("top_ten_gainers")
        
        if payload is None:
            logger.info('Not found in cache, serving from database')
            query = (
                db_session.execute(
                    """WITH p AS (
                    SELECT CompanyId,
                    100 * (MAX(CASE WHEN rn = 1 THEN Close END) / MAX(CASE WHEN rn = 2 THEN Close END) - 1) DayGain
                    FROM (
                    SELECT *, ROW_NUMBER() OVER (PARTITION BY CompanyId ORDER BY Date DESC) rn
                    FROM DimCompanyPrice
                    )
                    WHERE rn <= 2
                    GROUP BY CompanyId
                    ORDER BY DayGain DESC
                    LIMIT 10
                    )
                    SELECT *
                    FROM p
                    JOIN Company ON p.CompanyID = Company.ID"""
                )    
            )
            Gain = namedtuple('Gain', query.keys())
            gains = [Gain(*q) for q in query.fetchall()]
            payload = [[g.Symbol, g.Security, g.DayGain] for g in gains]
            r.set('top_ten_gainers', json.dumps(payload))
            total_payload['gainers'] = payload
        else:
            logger.info('Found in cache, serving from redis')
            total_payload['gainers'] = json.loads(payload)
            
        logger.info('query finished in {}'.format(time.perf_counter() - t0))

        return 0

    def _top_ten_losers():

        t0 = time.perf_counter()
        
        r = redis.Redis(host=current_app.config['REDIS_HOST'], port=6379, db=0)
        payload = r.get("top_ten_losers")
        
        if payload is None:
            logger.info('Not found in cache, serving from database')
            query = (
                db_session.execute(
                    """WITH p AS (
                    SELECT CompanyId,
                    100 * (MAX(CASE WHEN rn = 1 THEN Close END) / MAX(CASE WHEN rn = 2 THEN Close END) - 1) DayLose
                    FROM (
                    SELECT *, ROW_NUMBER() OVER (PARTITION BY CompanyId ORDER BY Date DESC) rn
                    FROM DimCompanyPrice
                    )
                    WHERE rn <= 2
                    GROUP BY CompanyId
                    ORDER BY DayLose ASC
                    LIMIT 10
                    )
                    SELECT *
                    FROM p
                    JOIN Company ON p.CompanyID = Company.ID"""
                )    
            )
            Loser = namedtuple('Loser', query.keys())
            losers = [Loser(*q) for q in query.fetchall()]
            payload = [[l.Symbol, l.Security, l.DayLose] for l in losers]
            r.set('top_ten_losers', json.dumps(payload))
            total_payload['losers'] = payload
        else:
            logger.info('Found in cache, serving from redis')
            total_payload['losers'] = json.loads(payload)
            
        logger.info('query finished in {}'.format(time.perf_counter() - t0))
            
        return 0
    
    
    def _return_price_ytd():

        year = date.today().year-1
        month = 12
        day = monthrange(year, month)[1]
        year_begin = date(year, month, day)

        query = (
            db_session.query(Index).all()
        )
        
        for row in query:
            q = (
                db_session.query(DimIndexPrice).\
                filter(DimIndexPrice.IndexID==row.ID).\
                filter(DimIndexPrice.Date>=year_begin).\
                order_by(DimIndexPrice.Date.asc()).\
                first()
            )
            logger.info('symbol {}'.format(row.Symbol))
            logger.info('close price {}'.format(q.Close))
            old_price = q.Close
            cur_price = index_payload[str(row.Symbol)]['current_price']
            index_payload[str(row.Symbol)]['ytd_percent_change'] = (cur_price - old_price) / old_price * 100
            total_payload['indexes'] = index_payload
            
        return 0

    _return_price_1day()
    _return_price_ytd()
    _top_ten_gainers()
    _top_ten_losers()
    
    return render_template('index.html', total_payload=total_payload, enumerate=enumerate)

