import functools

from flask import Blueprint, flash, g, redirect, render_template, request, session, url_for
from werkzeug.security import check_password_hash, generate_password_hash

from project.log import logger
from project.models import User
from project.crud import create_user, check_user_email, check_user_phone, check_user_id, create_portfolio_balance

auth_blueprint = Blueprint('auth', __name__, url_prefix='/auth')



@auth_blueprint.route('/register', methods=('GET', 'POST'))
def register():

    #user_id is some how returning a 9 when db is empty
    # if /logout is not fetched g will persist
    user_id = session.get('user_id')
    if user_id:
        return redirect(url_for('dashboard.home_get'))
        
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        phone = request.form['phone']
        password = request.form['password']
        valpassword = request.form['valpassword']
        error = None
        
        # if empty input, return error
        if not name:
            error = 'Name is required'
            flash(error)
        elif not email:
            error = 'Email is required.'
            flash(error)
        elif not password:
            error = 'Password is required.'
            flash(error)
        elif not phone:
            error = 'Phone is required.'
            flash(error)
        elif not valpassword:
            error = 'Password is required.'
            flash(error)
        elif valpassword != password:
            error = 'Password does not match.'
            flash(error)
        elif check_user_email(email):
            error = 'Email exists. Check your info or sign in.'
            flash(error)
        elif check_user_phone(phone):
            error = 'Phone exists. Check your info or sign in.'
            flash(error)

        # get columns of table minus the primary key 
        keys = [column.name for column in User.__table__.columns][1:]

        if error is None:
            # create new user 
            _password = generate_password_hash(password,method='pbkdf2:sha256', salt_length=8)
            _user = list(zip(keys, [name, email, phone, _password]))
            user = dict(_user)
            create_user(user)

            # check that user is in db
            query = check_user_email(user['Email'])
            session['user_id'] = query.ID

            # enqueue task to send email
            from project.tasks import send_new_user_email
            send_new_user_email.delay(query.Name, query.Email)

            # init user's portfolio
            portfolio_balance = {}
            portfolio_balance['PortfolioID'] = session.get('user_id')
            portfolio_balance['Balance'] = 0.0
            create_portfolio_balance(portfolio_balance)
                
            return redirect(url_for('dashboard.home_get'))
        
    return render_template('auth/register.html')


@auth_blueprint.route('/login', methods=('GET', 'POST'))
def login():

    user_id = session.get('user_id')
    if user_id:
        return redirect(url_for('dashboard.home_get'))
    
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        error = None
        user = check_user_email(email)

        #check if empty database first
        if user is None:
            error = 'Incorrect email or password.'
            flash(error)
            return redirect(url_for('auth.register'))
        elif not user.Email or not check_password_hash(user.Password, password):
            error = 'Incorrect email or password.'
            flash(error)
            return redirect(url_for('auth.register'))

        if error is None:
            session.clear()
            session['user_id'] = user.ID
            return redirect(url_for('dashboard.home_get'))

    return render_template('auth/login.html')    


@auth_blueprint.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index.index'))


@auth_blueprint.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id', None)
    if user_id is None:
        g.user = None
    else:
        g.user = check_user_id(user_id)


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view

