import os

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# https://docs.sqlalchemy.org/en/14/core/engines.html
_database_uri = os.environ['DATABASE_URL']
engine = create_engine(_database_uri, echo=False)

# https://docs.sqlalchemy.org/en/14/orm/session_basics.html
# sessionmaker is shorthand for a python context manager w/ try: / except: / else: block
# https://stackoverflow.com/questions/6519546/scoped-sessionsessionmaker-or-plain-sessionmaker-in-sqlalchemy#:~:text=the%20scoped_session()%20function%20is,localized%20to%20a%20single%20thread.
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import project.models
    Base.metadata.create_all(bind=engine)


def recreate_db():
    import project.models
    Base.metadata.drop_all(bind=engine)
