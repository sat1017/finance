import os

# from celery import Celery
from flask import Flask
from flask_caching import Cache
from flask_celeryext import FlaskCeleryExt

from project.celery_utils import make_celery
from project.config import config

# create a celery app
# https://github.com/inveniosoftware/flask-celeryext/blob/v0.3.4/flask_celeryext/ext.py#L20
# https://github.com/inveniosoftware/flask-celeryext/blob/v0.3.4/flask_celeryext/ext.py#L33
# https://flask-celeryext.readthedocs.io/en/latest/
# When we instantiated ext_celery, we passed the custom application factory make_celery to it.
# If we hadn't done this, the FlaskCeleryExt would create Celery app for us automatically,
# which is not recommended since we'll be running shared tasks.
ext_celery = FlaskCeleryExt(create_celery_app=make_celery)

def create_app(config_name=None):
    if config_name is None:
        config_name = os.environ.get("FLASK_CONFIG", "development")

    cache = Cache(config={'CACHE_TYPE': 'SimpleCache'})
    
    # instantiate the app
    app = Flask(__name__)
    cache.init_app(app)
    
    # set up config
    app.config.from_object(config[config_name])
    
    # initialize the celery app inside the flask app context
    ext_celery.init_app(app)
    
    # set up extensions
    with app.app_context():
        from . import manage
    

    # initialize db from function
    from project.database import init_db
    init_db()

    # register blueprints
    from project.auth import auth_blueprint
    from project.dashboard import dashboard_blueprint
    from project.data import data_blueprint
    from project.search import search_blueprint
    from project.index import index_blueprint
    from project.tasks import tasks_blueprint
    from project.about import about_blueprint
    
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(data_blueprint)
    app.register_blueprint(dashboard_blueprint)
    app.register_blueprint(search_blueprint)
    app.register_blueprint(index_blueprint)
    app.register_blueprint(tasks_blueprint)
    app.register_blueprint(about_blueprint)


    from project.database import db_session
    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db_session.remove()

    return app
