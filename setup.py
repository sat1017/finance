from setuptools import find_packages, setup

setup(
    name='app',
    version='1.0.0',
    package=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
    ],
    extras_requires={
        'test': [
            'pytest',
            'coverage',
        ]},
)
