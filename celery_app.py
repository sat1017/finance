# We did not use eventlet.monkey_patch, so the Celery worker process still has full access to the Python standard library.

from project import create_app, ext_celery

app = create_app()
celery = ext_celery.celery

# TODO
# need to update the start scripts to use the cli functions

''' A custom command called celery_worker that uses run_process to monitor the
"project" directory for changes to any Python file recursively. run_worker and
run_beat are a callback function that gets called when a change occurs.
'''
@app.cli.command("celery_worker")
def celery_worker():
    from watchgod import run_process
    import subprocess
    
    def run_worker():
        subprocess.call(
            ["celery", "-A", "celery_app.celery", "worker", "--loglevel=info", '-Q', 'high_priority,default']
        )

    run_process("./project", run_worker)
