#!/bin/sh
# called by Dockerfile

# go to working dir
cd /usr/src/app

# set up enviornment
# source venv/bin/activate
source .env

# start production server (gunicorn)
# exec gunicorn -b :5000 --access-logfile - --error-logfile - wsgi:app

# start dev server
python3 -m flask run --host=0.0.0.0
