import os
import tempfile

import pytest
from app import create_app
from app.db import get_db, init_db

# https://jeffknupp.com/blog/2016/03/07/improve-your-python-the-with-statement-and-context-managers//
with open(os.path.join(os.path.dirname(__file__), 'data.sql'), 'rb') as f:
    _data_sql = f.read().decode('utf8')


@pytest.fixture
def app():
    db_fd, db_path = tempfile.mkstemp()

    # passes dict of configuration to test_config to load in from mapping
    app = create_app({
        'TESTING': True,
        'DATABASE': db_path,
    })

    # redirects current_app to point to this test application
    # opens and closes resource automatically with the with statement
    with app.app_context():
        init_db()
        get_db().executescript(_data_sql)

    # what does yield do?
    yield app

    # closes resoruces of tempfile
    os.close(db_fd)
    os.unlink(db_path)


# interface to the application when testing
@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()


class AuthActions(object):
    def __init__(self, client):
        self._client = client

    # login information is sent to test client via a post request
    def login(self, email='example@test.com', password='test'):
        return self._client.post(
            '/auth/login',
            data={'email': email, 'password': password}
        )

    def logout(self):
        return self._client.get('/auth/logout')


@pytest.fixture
def auth(client):
    return AuthActions(client)
