import pytest
from flask import g, session
from app.db import get_db


def test_register(client, app):
    ''' register view should:
    - render on successfull GET request
    - redirect you to /login if user registered correctly
    - user info should be saved in db after successfull registration
    '''

    assert client.get('/auth/register').status_code == 200
    response = client.post(
        '/auth/register', data={'email': 'a',
                                'phone': '1',
                                'password': 'a',
                                'valpassword': 'a'}
    )
    assert 'http://localhost/auth/login' == response.headers['Location']

    with app.app_context():
        assert get_db().execute(
            "SELECT * from user WHERE email = 'a'",
        ).fetchone() is not None


@pytest.mark.parametrize(('email', 'phone', 'password', 'valpassword', 'message'), (
    ('', '', '', '',  b'Email is required.'),
    ('a', '1', '', '', b'Password is required.'),
    ('a', '', 'p', '', b'Phone is required'),
    ('a', '1', 'p', '', b'Password is required'),
    ('a', '1', 'p', 'p2', b'Password does not match.'),
    ('example@test.com', '1', 'p', 'p',
     b'Email example@test.com is already registered.'),
    ('a', '1234567890', 'p', 'p', b'Phone 1234567890 is already registered.'),
))
def test_register_valdiate_input(client, email, phone, password, valpassword, message):
    ''' invalid data should display error when validated on the backend '''
    response = client.post(
        '/auth/register',
        data={'email': email, 'phone': phone,
              'password': password, 'valpassword': valpassword}
    )
    assert message in response.data


def test_login(client, auth):
    assert client.get('/auth/login').status_code == 200
    response = auth.login()
    assert response.headers['Location'] == 'http://localhost/'

    with client:
        client.get('/')
        assert session['user_id'] == 1
        assert g.user['email'] == 'example@test.com'


@pytest.mark.parametrize(('email', 'password', 'message'), (
    ('a', 'test', b'Incorrect email.'),
    ('example@test.com', 'a', b'Incorrect password.'),
))
def test_login_validate_input(auth, email, password, message):
    reponse = auth.login(email, password)
    assert message in reponse.data


def test_logout(client, auth):
    auth.login()

    with client:
        auth.logout()
        assert 'user_id' not in session
