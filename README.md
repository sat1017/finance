# About the Author
For an overview of who I am and what fundamental knowledge I learned before this application please visit https://stephen-tomaszewski.com/

# About the app
This application is an asset portfolio that mimicks the functionality of websites like Vanguard. It is written in Python and uses Flask and a SQlite database on the backend. The application schedules long running tasks via Celery and Redis. The type of tasks are email notifications for portfolio changes and batch API calls to update the database source data. Services were containerized for ease of deployment using docker. For production, gunicorn was used for concurrency and Nginx was used as a reverse proxy.

An attempt was made to use best practices but in the effort of time, there are many things left to be desired. Not all edge cases are dealt with so please don't try to break the app. That said, a large amount of time was spent getting the app to it's current state and too many things were learned along the way to acurately try to list.

## API task schedule
|Sunday                      |Monday                      |Tuesday                     |Wednesday                   |Thursday                    |Friday                      |Saturday                    |
|----------------------------|----------------------------|----------------------------|----------------------------|----------------------------|----------------------------|----------------------------|
|get_sp_500_list: All        |get_sp_500_list: All        |get_sp_500_list: All        |get_sp_500_list: All        |get_sp_500_list: All        |get_sp_500_list: All        |get_sp_500_list: All        |
|get_index_historical: All   |get_index_historical: All   |get_index_historical: All   |get_index_historical: All   |get_index_historical: All   |get_index_historical: All   |get_index_historical: All   |
|get_company_historical: Half|get_company_historical: Half|get_company_historical: Half|get_company_historical: Half|get_company_historical: Half|get_company_historical: Half|get_company_historical: Half|
|get_company_info: None      |get_company_info: 1/5th     |get_company_info: 1/5th     |get_company_info: 1/5th     |get_company_info: 1/5th     |get_company_info: 1/5th     |get_company_info: None      |
|Total API Calls: 252 of 500 |Total API Calls: 353 of 500 |Total API Calls: 353 of 500 |Total API Calls: 353 of 500 |Total API Calls: 353 of 500 |Total API Calls: 353 of 500 |Total API Calls: 252 of 500 |

## Flask CLI 
Commands are registered @ `//project/manage.py`
Run `flask --help` to get list of commands\
Run `flask <command>` to run a command

```
Commands:
  _create_db               Drop all tables & create new tables.
  _get_company_historical  Get company price data from Alpha Vantage.
  _get_company_info        Get company info from Alpha vantage.
  _get_index_historical    Get index price data from Alpha Vantage.
  _get_sp500_list          Scrape the S&P500 constituents list from...
  _init_app                Initialize the app.
```

# Deployment
## Remote Deployment
1. register domain name (google domains)
2. add cloudfare DNS to domain regestrar
3. create a VPS (linode)
4. add VPS instance IP to cloudfare's DNS management system
   - add a CNAME for www prefix to point to the domain name
   - add an A for the domain name to point to the instance IP
5. create key and disable password login
   - create key on local machine `ssh-keygen -t ed25519`
   - copy public key to remote machine `ssh-copy-id root@<YOUR_INSTANCE_IP>`
   - add public key ` nano ~/.ssh/authorized_keys`
   - Verify ssh works `ssh -o StrictHostKeyChecking=no root@<YOUR_INSTANCE_IP> whoami`
   - verify ssh works `ssh root@<YOUR_INSTANCE_IP>`
6. add cloudfare origin cert to VPS
   - (CloudFare site) Navigate to SSl/TS then Origin Server then click create certificate
   - (remote) `sudo nano /etc/ssl/cert.pem` and paste the cert
   - (remote) `sudo nano /etc/ssl/key.pem` and paste the key
7. configure Nginx to proxy to gunicorn (Obsolete)
   - install Nginx (VPS) `apt install nginx`
   - remove the default config (VPS) `rm/etc/nginx/conf.d/default.conf`
   - copy over the web server's config `COPY nginx.conf /etc/nginx/conf.d`
   - add remote IP address to server_name variable
   - set upstream server to `web:5000` as nginx will resolve the web proxy IP and 5000 is the exposed port for the web container
8. clone the project from the remote version control system to the VPS
   - `git clone --branch master https://gitlab.com/sat1017/finance.git`
9. secure copy .env directory
   - (local) `scp -r .env root@<YOUR_INSTANCE_IP>:finance`
10. add the logger log file to the project even though it is not used to prevent error in production
11. allow http & https
   - (remote) `ufw enable`
   - (remote) `ufw allow http`
   - (remote) `ufw allow https`
12. start up the docker containers
   - (remote) `sudo docker-compose -f docker-compose.prod.yml -p prod up -d --build`
13. init app from inside web container
    - (remote) `docker exec -it [CONTAINER_ID] sh`
	- (remote) `flask _init_app`

## Production Deployment
```
sudo docker-compose -f docker-compose.prod.yml -p prod up -d --build
```

## Local Deployment
Terminal 1
```
source venv/bin/activate && \
source .env/.dev.local && \
flask run
```
Terminal 2
```
sudo docker run -p 6379:6379 --name app-redis -d redis && \
sudo docker start app-redis && \
source venv/bin/activate && \
source .env/.dev.local && \
celery -A celery_app.celery worker --loglevel=info
```
Terminal 3
```
source venv/bin/activate && \
source .env/.dev.local && \
celery -A celery_app.celery beat --loglevel=info
```

# Git workflow
Create a new branch
```
git pull origin development
git checkout development -b [new-feature-branch]
```
Save the changes
```
git commit -am 'change-description'
git push -u origin [new-feature-branch]
```
Merge the changes
```
git checkout development
git pull
git pull origin [new-feature-branch]
git push origin development
git push origin --delete [new-feature-branch]
git branch -d [new-feature-branch]
```

# References
## Docker
Build
```
sudo docker-compose -f docker-compose.prod.yml -p prod up -d --build
```
Logs
```
sudo docker-compose -f docker-compose.prod.yml -p prod logs -f
```
Cleanup
```
sudo docker-compose -f docker-compose.prod.yml -p prod stop
```
Delete containers and volumes
```
sudo docker-compose -f docker-compose.prod.yml -p prod down -v
```
Verify containers and volumes are deleted
```
sudo docker-compose -f docker-compose.prod.yml -p prod ps
```
Check error state for OOMKilled
```
sudo docker inspect [OPTIONS] Name|ID
```
Display the running processes of a container
```
sudo docker top CONTAINER [ps OPTIONS]
```
Enter a container by Container ID and run the shell
```
sudo docker exec -it [CONTAINER_ID] sh
```
Use the redis container
```
sudo docker exec -it app-redis bash
```
```
root@somecontainer# redis-cli
```

## Linux 
See what ports are open
```
netstat -tulpen
```
How to find a process running python\
`ps -fA | grep python`

## Emacs
Monitor logs\
`$ less +F *.log` or `$ tail -F *.log` or can use `M-x auto-revert-tail-mode` while in the .log buffer\
Multiline indent\
`C-x TAB` or `C-c >`\
Open multiple shells\
`C-u M-x eshell` or`M-x rename-eshell`\
Evaluate a buffer on the fly\
`M-x eval-buffer`\
Evaluate a LISP function\
`C-x C-e`

## Understanding SQLAlchemy query statements
```
# how to view the raw sql
# https://stackoverflow.com/questions/68278637/sqlalchemy-single-query-to-return-columns-from-two-tables/68291933#68291933
from sqlalchemy.dialects import sqlite
print(combined_query2.statement.compile(dialect=sqlite.dialect()))

query = db_session.query(DimCompanyInfo.id) # -> <class 'sqlalchemy.orm.query.Query'>
print(type(query))
query_unpacked = list(zip(*query))
# OR
query_unpacked =[value for (value,) in query]


# can access the columns of each row via attribute calls to the mapped sa obj
query1 = db_session.query(DimCompanyInfo) # -> <class 'sqlalchemy.orm.query.Query'>
print(type(query1))
# print(query1)
for instance in query1:
    print(type(instance))     # -> <class 'app.models.DimCompanyInfo'>
    print(instance.Symbol)
    break
# OR
# can unpack all the rows into list of dicts where the keys are mapped to the sa table columns
rows = [row2dict(row) for row in query1]


# since query returns list of rows, need to loop through rows to access attributes
query2 = db_session.query(DimCompanyInfo).all() # -> <class 'list'>
print(type(query2))
# print(query2)
for instance in query2:
    print(type(instance))     # -> <class 'app.models.DimCompanyInfo'>
    print(instance.Symbol, instance.company_id)
    break


query3 = db_session.query(DimCompanyInfo).filter_by(Symbol='IBM') # -> <class 'sqlalchemy.orm.query.Query'>
print(type(query3))
for instance in query3:
    print(type(instance))     # -> <class 'app.models.DimCompanyInfo'>
    print(instance.Symbol, instance.company_id) # specific attribute
    print(instance.__dict__.items()) # generalized attribute retrevial
    break


# first() returns the first record matching our query, despite how many
# records match the query (what constitutes "first" depends on how your
# table is sorted). This is the equivalent of adding LIMIT 1 to a SQL
# query. As a result, the Python type to be returned would be
# DimCompanyInfo.
# can use obj __dict__ methods to access key, values directly 
query4 = db_session.query(DimCompanyInfo).filter_by(Symbol='IBM').first() # -> <class 'app.models.DimCompanyInfo'>
print(type(query4))
print(query4.__dict__)
print(query4.__dict__.items())
print(query4.__dict__.keys())
print(query4.__dict__.values())
print(query4.__dict__.items())  # generalized attribute retrevial
for query_dict in query4.__dict__.items(): # iter through each column
    print(query_dict)
    break


# all() will return all records which match our query as a list of
# objects. If we were to use all on the query above, we would
# receive all DimCompanyInfo records with the Python data type
# List[DimCompanyInfo].
query5 = db_session.query(DimCompanyInfo).filter_by(Symbol='IBM').all() # -> <class 'list'>
print(type(query5))
print(query5)
for instance in query5:
    print(type(instance))
    print(instance.__dict__.items())
```
## How to convert tuples to dict
```
def Convert(tup, di={}):
    di = dict(tup)
    return di
	
query = db_session.query(DimCompanyInfo).filter_by(Symbol=q).first()
temp = [tup for tup in query.__dict__.items()]
row = Convert(temp)
```
## How to convert sa row to dict
```
# https://stackoverflow.com/questions/1958219/convert-sqlalchemy-row-object-to-python-dict
# this method removes the first key which is a sa instance object 
def row2dict(row) -> dict:
    d = {}
    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))

    return d
	
query = db_session.query(DimCompanyInfo).filter_by(Symbol=q).first()
row = row2dict(query)
```
## How to get list of sa tables
```
# https://docs.sqlalchemy.org/en/14/core/metadata.html
for t in Base.metadata.sorted_tables:
    print(t.name)
```
## How to get table by name
```
from models import User

User.__table__.name
User.__tablename__
User.__table__
```

## How to generalize unpacking a tuple
https://stackoverflow.com/questions/44355850/python-sqlalchemy-convert-lists-of-tuples-to-list-of-atomic-values

https://stackoverflow.com/questions/46794115/passing-a-tuple-in-args

https://stackoverflow.com/questions/48466959/query-for-list-of-attribute-instead-of-tuples-in-sqlalchemy

Can use zip, chain, for loop, or ```unpacked = [value for (value,) in tuple]```

## Python source code
/lib/python3.8/
Can use `grep -R "some-string" some-file-path` to search for functions and learn more

# Tools Learned
git
gitlab
Python
Flask
logging
error/exception handling
command line functions for init
sockets
message broker
scheduled tasks
task routing
retrying tasks with exp backoff
eventlet greening
gunicorn server
SQLAlchemy
SQL
joins
indexes
docker/docker-compose
bootstrap
html
css
web sockets and long polling
JS charts
